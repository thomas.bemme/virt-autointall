#!/bin/bash 
MIRROR="debian.inf.tu-dresden.de"
MAC="RANDOM"
SIZE=8

function help {
    echo -e "Usage:\n\t $(basename $0) -h <GUEST_NAME> [-m <MAC> -s <Disksize in GB>]" >&2
    exit 1
  }

while getopts h:m:s: flag
do
    case "${flag}" in
        h) NAME=${OPTARG};;
        m) MAC=${OPTARG};;
        s) SIZE=${OPTARG};;
        *) help;;
    esac
done

if [[ -z "$NAME" || "$SIZE" -lt "8" || -z "$MAC" ]]; then
  help
fi

echo -e "Virtual machine will be created wit the following details:\n\tName: $NAME\n\tDisk size: ${SIZE}GB\n\tMac address: $MAC"
echo "Please note that zram will be used with 50% of RAM size, up to 4GB"
read -p "Continue?" -sn1

virt-install --virt-type kvm --name ${NAME} --location https://${MIRROR}/debian/dists/bookworm/main/installer-amd64/ --os-variant debian12 --memory 1024 --graphics none --noautoconsole --extra-args "hostname=$NAME domain="fritz.box" console=tty0 console=ttyS0,115200n8 serial auto=true url=http://kbnetcloud.de/debian/preseed" --disk pool=default,size=${SIZE} --network bridge=br0,mac=${MAC},model=virtio --controller usb,model=none --memorybacking=source.type=memfd,access.mode=shared

finished=0;

while [ "$finished" = 0 ]; do
  sleep 5;
  if [ `virsh list | grep "$NAME" | wc -c` -eq 0 ];
  then
    echo "setup finished, starting vm $NAME"
    finished=1;
    virsh setmem $NAME 512M --config
    virsh start $NAME 
  fi
done

