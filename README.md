# Virt-autoinstall

## Virt-autoinstall
Shell scripts for auto installing KVM debian VMs via virt-install

## Description
Simple automation for virt-install via preseed config, postinstall and fistboot scripts
