#!/bin/sh

USER="YOURUSER"
SSHPUBKEY="YOURPUBLICSSHKEY"
NTPSERVER="YOURTIMESERVER"
DOMAIN="YOURDOMAIN"

# Set Grub console output for virsh console
echo $(date --rfc-3339=s) 'Edit /etc/default/grub console settings'
sed -i "s/^GRUB_CMDLINE_LINUX_DEFAULT.*/GRUB_CMDLINE_LINUX_DEFAULT=\"splash console=ttyS0\"/g" /etc/default/grub
/usr/sbin/update-grub

# SSH key
echo $(date --rfc-3339=s) "Adding ssh key for user $USER"
mkdir -vm700 "/home/$USER/.ssh"
echo $SSHKEY > "/home/$USER/.ssh/authorized_keys"
chown -vR "$USER:$USER" "/home/$USER/.ssh"

# Chrony
echo $(date --rfc-3339=s) 'Configuring chrony to use $NTPSERVER'
sed -i "s/^pool.*/server $NTPSERVER iburst/g" /etc/chrony/chrony.conf

# Set Logwatch to weekly
echo $(date --rfc-3339=s) 'Configuring logwatch'
mv -v /etc/cron.daily/00logwatch /etc/cron.weekly/
cp -v /usr/share/logwatch/default.conf/logwatch.conf /etc/logwatch/conf/
mkdir -v /var/cache/logwatch
sed -i '/^Range/s/yesterday/\"between -7 days and -1 days\"/g' /etc/logwatch/conf/logwatch.conf


# Setup mail aliases
echo $(date --rfc-3339=s) 'Set mail aliases'
echo "root:$(hostname)@$DOMAIN" > /etc/aliases
echo "default:$(hostname)@$DOMAIN" >> /etc/aliases

# Configure mail
echo $(date --rfc-3339=s) 'Configuring msmtp'
printf "# Set default values for all following accounts.\ndefaults\n#auth           on\n#tls            on\n#tls_trust_file /etc/ssl/certs/ca-certificates.crt\nlogfile        ~/.msmtp.log\n\n#kbnet\naccount        kbnet\nhost           mail\nport           25\nfrom           $(hostname)@$DOMAIN\nset_from_header on\n\n# Set a default account\naccount default : kbnet\n#\naliases               /etc/aliases" > /etc/msmtprc

# Configure Munin
echo $(date --rfc-3339=s) 'Configuring munin'
echo 'allow ^192\.168\.178\.17$' >> /etc/munin/munin-node.conf

# Configure APT
echo $(date --rfc-3339=s) 'Configuring apt for unattended updates'
printf "APT::Periodic::Update-Package-Lists \"1\";\nAPT::Periodic::Download-Upgradeable-Packages \"1\";\nAPT::Periodic::AutocleanInterval \"7\";\nAPT::Periodic::Unattended-Upgrade \"1\";\n" > /etc/apt/apt.conf.d/20auto-upgrades
sed -i "s/\/\/Unattended-Upgrade::Mail \"\";/Unattended-Upgrade::Mail \"root\";/g" /etc/apt/apt.conf.d/50unattended-upgrades
sed -i "s/\/\/Unattended-Upgrade::Automatic-Reboot \"false\";/Unattended-Upgrade::Automatic-Reboot \"true\";/g" /etc/apt/apt.conf.d/50unattended-upgrades

# Setup first boot
echo $(date --rfc-3339=s) 'Setup firstboot script'
wget -O /firstboot.sh "https://$DOMAIN/debian/firstboot.sh"
wget -O /etc/systemd/system/firstboot.service "https://$DOMAIN/debian/firstboot.service"
chmod +x /firstboot.sh
systemctl enable firstboot.service
