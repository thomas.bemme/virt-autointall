#!/bin/bash

DOMAIN="YOURDOMAIN"
MAIL="YOURMAILSERVER"
LOGSERVER="YORLOGSERVER"
SSHPRIV="YOURSSHKEY"
SSHPUB="YOURPUBKEY"
GIT="YOURLOCALGIT"

#Set UFW rules
echo $(date --rfc-3339=s) 'Set UFW rules and enable on boot'
/usr/sbin/ufw allow ssh
/usr/sbin/ufw allow from $LOGSERVER to any proto tcp port 4949
/usr/sbin/ufw allow from $LOGSERVER to any proto tcp port 9100
/usr/sbin/ufw --force enable

echo $(date --rfc-3339=s) "Check if $DOMAIN can be resolved"
until host $DOMAIN; do
  echo $(date --rfc-3339=s) 'Host not found, retrying in 5 seconds...'
  sleep 5
done

# fish shell
echo $(date --rfc-3339=s) "Install and configure fish shell"
chsh riza -s /usr/bin/fish
mkdir -vp /home/riza/.config/fish/
wget -O /home/riza/.config/fish/config.fish "https://$DOMAIN/debian/config.fish"
chown -R riza:riza /home/riza/.config

# vimrc
echo $(date --rfc-3339=s) 'Set vim defaults and default editor'
printf "set tabstop=2\nset shiftwidth=2\nset expandtab\nset shiftround\ncolorscheme koehler\nset history=100\nset hlsearch\nset ignorecase\nset wildmenu\nset number" > /etc/vim/vimrc.local
/usr/bin/update-alternatives --set editor /usr/bin/vim.basic

# Wait for network
echo $(date --rfc-3339=s) "Check if $MAIL can be resolved"
until host $MAIL; do
  echo $(date --rfc-3339=s) 'Host not found, retrying in 5 seconds...'
  sleep 5
done

# Enable remote git etckeeper
echo $(date --rfc-3339=s) "Configure etckeeper for git.$DOMAIN"
mkdir -vm 0700 /root/.ssh
echo -e $SSHPRIV > /root/.ssh/id_rsa
chmod 0600 -v /root/.ssh/id_rsa
echo -e $SSHPUB > /root/.ssh/known_hosts
git -C /etc/ branch -m master main
git -C /etc/ remote add origin $GIT:etckeeper/$(hostname).git
git -C /etc push --set-upstream origin main
sed -i /PUSH_REMOTE/s/\"\"/\"origin\"/g /etc/etckeeper/etckeeper.conf
/usr/bin/etckeeper commit "Firstboot changes"

# Disable firstboot
echo $(date --rfc-3339=s) 'Disable firstboot and cleanup'
systemctl disable firstboot.service
rm -rf /etc/systemd/system/firstboot.service
rm -f /firstboot.sh

# Send finishing mail
echo $(date --rfc-3339=s) 'Send finishing mail'
printf "Subject: Installation complete on $(hostname)\nInstallation was completed on $(date)\n\nNetwork config:\n\n $(hostname -f)\n$(hostname -I)" | msmtp root
